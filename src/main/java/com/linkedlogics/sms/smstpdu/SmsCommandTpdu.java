package com.linkedlogics.sms.smstpdu;

public interface SmsCommandTpdu extends SmsTpdu {

    public boolean getUserDataHeaderIndicator();

    public boolean getStatusReportRequest();

    public int getMessageReference();

    public ProtocolIdentifier getProtocolIdentifier();

    public CommandType getCommandType();

    public int getMessageNumber();

    public AddressField getDestinationAddress();

    public int getCommandDataLength();

    public CommandData getCommandData();
}
