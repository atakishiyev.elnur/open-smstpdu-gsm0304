package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;

public enum DataCodingGroup implements Serializable {
    GeneralGroup,
    MessageMarkedForAutomaticDeletionGroup,
    MessageWaitingIndicationGroupDiscardMessage,
    MessageWaitingIndicationGroupStoreMessage,
    DataCodingMessageClass,
    Reserved;
}
