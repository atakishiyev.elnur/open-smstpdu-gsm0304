package com.linkedlogics.sms.smstpdu;

import org.mobicents.protocols.asn.AsnOutputStream;

public interface AddressField {

    public TypeOfNumber getTypeOfNumber();

    public NumberingPlanIdentification getNumberingPlanIdentification();

    public String getAddressValue();

    public void encodeData(AsnOutputStream stm) throws Exception;
}
