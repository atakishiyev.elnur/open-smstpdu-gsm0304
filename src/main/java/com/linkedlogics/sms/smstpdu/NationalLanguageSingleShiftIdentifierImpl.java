package com.linkedlogics.sms.smstpdu;

import com.linkedlogics.sms.datacoding.NationalLanguageIdentifier;

public class NationalLanguageSingleShiftIdentifierImpl extends Gsm7NationalLanguageIdentifierImpl implements NationalLanguageSingleShiftIdentifier {

    public NationalLanguageSingleShiftIdentifierImpl(NationalLanguageIdentifier nationalLanguageCode) {
        super(nationalLanguageCode);
    }

    public NationalLanguageSingleShiftIdentifierImpl(byte[] encodedInformationElementData) {
        super(encodedInformationElementData);
    }

    @Override
    public int getEncodedInformationElementIdentifier() {
        return UserDataHeader._InformationElementIdentifier_NationalLanguageSingleShift;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NationalLanguageSingleShiftIdentifier [");
        sb.append("nationalLanguageCode=");
        sb.append(this.getNationalLanguageIdentifier());
        sb.append("]");

        return sb.toString();
    }
}
