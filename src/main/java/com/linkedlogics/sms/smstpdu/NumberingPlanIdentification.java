package com.linkedlogics.sms.smstpdu;

public enum NumberingPlanIdentification {

    Unknown(0),
    ISDNTelephoneNumberingPlan(1),
    DataNumberingPlan(3),
    TelexNumberingPlan(4),
    ServiceCentreSpecificPlan1(5),
    ServiceCentreSpecificPlan2(6),
    NationalNumberingPlan(8),
    PrivateNumberingPlan(9),
    ERMESNumberingPlan(10),
    Reserved(15);
    private int code;

    private NumberingPlanIdentification(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static NumberingPlanIdentification getInstance(int code) {
        switch (code) {
            case 0:
                return Unknown;
            case 1:
                return ISDNTelephoneNumberingPlan;
            case 3:
                return DataNumberingPlan;
            case 4:
                return TelexNumberingPlan;
            case 5:
                return ServiceCentreSpecificPlan1;
            case 6:
                return ServiceCentreSpecificPlan2;
            case 8:
                return NationalNumberingPlan;
            case 9:
                return PrivateNumberingPlan;
            case 10:
                return ERMESNumberingPlan;
            default:
                return Reserved;
        }
    }
}
