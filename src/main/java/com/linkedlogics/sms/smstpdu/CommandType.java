package com.linkedlogics.sms.smstpdu;

public interface CommandType {

    public int getCode();

    public CommandTypeValue getCommandTypeValue();

}
