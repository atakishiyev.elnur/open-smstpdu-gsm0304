package com.linkedlogics.sms.smstpdu;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.mobicents.protocols.asn.AsnException;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;
import org.mobicents.protocols.asn.Tag;

public abstract class TbcdString {

    protected static int DIGIT_1_MASK = 0x0F;
    protected static int DIGIT_2_MASK = 0xF0;
    protected String data;
    protected int minLength;
    protected int maxLength;
    protected String _PrimitiveName;

    public TbcdString(int minLength, int maxLength, String _PrimitiveName) {
        this.minLength = minLength;
        this.maxLength = maxLength;
        this._PrimitiveName = _PrimitiveName;
    }

    public TbcdString(int minLength, int maxLength, String _PrimitiveName, String data) {
        this(minLength, maxLength, _PrimitiveName);

        this.data = data;
    }

    public int getTag() throws Exception {
        return Tag.STRING_OCTET;
    }

    public int getTagClass() {
        return Tag.CLASS_UNIVERSAL;
    }

    public boolean getIsPrimitive() {
        return true;
    }

    public void decodeAll(AsnInputStream ansIS) throws Exception {

        try {
            int length = ansIS.readLength();
            this._decode(ansIS, length);
        } catch (IOException e) {
            throw new Exception("IOException when decoding ");
        }
    }

    public void decodeData(AsnInputStream ansIS, int length) throws Exception {

        try {
            this._decode(ansIS, length);
        } catch (IOException e) {
            throw new Exception("IOException when decoding ");
        }
    }

    protected void _decode(AsnInputStream ansIS, int length) throws Exception, IOException {

        if (!ansIS.isTagPrimitive()) {
            throw new Exception("Error decoding ");
        }

        if (length < this.minLength || length > this.maxLength) {
            throw new Exception("Error decoding ");
        }

        try {
            this.data = decodeString(ansIS, length);
        } catch (IOException e) {
            throw new Exception("IOException when decoding ");
        }
    }

    public void encodeAll(AsnOutputStream asnOs) throws Exception {

        this.encodeAll(asnOs, this.getTagClass(), this.getTag());
    }

    public void encodeAll(AsnOutputStream asnOs, int tagClass, int tag) throws Exception {

        try {
            asnOs.writeTag(tagClass, this.getIsPrimitive(), tag);
            int pos = asnOs.StartContentDefiniteLength();
            this.encodeData(asnOs);
            asnOs.FinalizeContent(pos);
        } catch (AsnException e) {
            throw new Exception("AsnException when encoding " + _PrimitiveName + ": " + e.getMessage(), e);
        }
    }

    public void encodeData(AsnOutputStream asnOs) throws Exception {

        if (this.data == null) {
            throw new Exception("Error while encoding the " + _PrimitiveName + ": data is not defined");
        }

        encodeString(asnOs, this.data);
    }

    public static String decodeString(AsnInputStream ansIS, int length) throws IOException, Exception {
        StringBuilder s = new StringBuilder();
        for (int i1 = 0; i1 < length; i1++) {
            int b = ansIS.read();

            int digit1 = (b & DIGIT_1_MASK);
            if (digit1 == 15) {
                // this is mask
            } else {
                s.append(decodeNumber(digit1));
            }

            int digit2 = ((b & DIGIT_2_MASK) >> 4);
            if (digit2 == 15) {
                // this is mask
            } else {
                s.append(decodeNumber(digit2));
            }
        }

        return s.toString();
    }

    public static void encodeString(AsnOutputStream asnOs, String data) throws Exception {
        char[] chars = data.toCharArray();
        for (int i = 0; i < chars.length; i = i + 2) {
            char a = chars[i];

            int digit1 = encodeNumber(a);
            int digit2;
            if ((i + 1) == chars.length) {
                // add the filler instead
                digit2 = 15;
            } else {
                char b = chars[i + 1];
                digit2 = encodeNumber(b);
            }

            int digit = (digit2 << 4) | digit1;


            asnOs.write(digit);

        }

    }

    protected static int encodeNumber(char c) throws Exception {
        switch (c) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case '*':
                return 10;
            case '#':
                return 11;
            case 'a':
                return 12;
            case 'b':
                return 13;
            case 'c':
                return 14;
            default:
                throw new Exception(
                        "char should be between 0 - 9, *, #, a, b, c for Telephony Binary Coded Decimal String. Received "
                        + c);

        }
    }

    protected static char decodeNumber(int i) throws Exception {
        switch (i) {
            case 0:
                return '0';
            case 1:
                return '1';
            case 2:
                return '2';
            case 3:
                return '3';
            case 4:
                return '4';
            case 5:
                return '5';
            case 6:
                return '6';
            case 7:
                return '7';
            case 8:
                return '8';
            case 9:
                return '9';
            case 10:
                return '*';
            case 11:
                return '#';
            case 12:
                return 'a';
            case 13:
                return 'b';
            case 14:
                return 'c';
            // case 15:
            // return 'd';
            default:
                throw new Exception();

        }
    }

    @Override
    public String toString() {
        return _PrimitiveName + " [" + this.data + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TbcdString other = (TbcdString) obj;
        if (data == null) {
            if (other.data != null) {
                return false;
            }
        } else if (!data.equals(other.data)) {
            return false;
        }
        return true;
    }
}
