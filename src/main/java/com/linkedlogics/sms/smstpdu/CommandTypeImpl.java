package com.linkedlogics.sms.smstpdu;

public class CommandTypeImpl implements CommandType {

    private final int code;

    public CommandTypeImpl(int code) {
        this.code = code;
    }

    public CommandTypeImpl(CommandTypeValue value) {
        this.code = value.getCode();
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public CommandTypeValue getCommandTypeValue() {
        return CommandTypeValue.getInstance(this.code);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("TP-Command-Type [");
        sb.append(this.code);
        sb.append("]");

        return sb.toString();
    }
}
