package com.linkedlogics.sms.smstpdu;

public interface ProtocolIdentifier {

    public int getCode();
}
