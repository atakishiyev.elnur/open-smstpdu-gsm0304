package com.linkedlogics.sms.smstpdu;

public interface ParameterIndicator {

    public int getCode();

    public boolean getTP_UDLPresence();

    public boolean getTP_DCSPresence();

    public boolean getTP_PIDPresence();
;
}
