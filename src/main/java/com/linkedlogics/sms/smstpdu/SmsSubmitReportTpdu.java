package com.linkedlogics.sms.smstpdu;

public interface SmsSubmitReportTpdu extends SmsTpdu {

    public boolean getUserDataHeaderIndicator();

    public FailureCause getFailureCause();

    public ParameterIndicator getParameterIndicator();

    public AbsoluteTimeStamp getServiceCentreTimeStamp();

    public ProtocolIdentifier getProtocolIdentifier();

    public DataCodingScheme getDataCodingScheme();

    public int getUserDataLength();

    public UserData getUserData();
}
