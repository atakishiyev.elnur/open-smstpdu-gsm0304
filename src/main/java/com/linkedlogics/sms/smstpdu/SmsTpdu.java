package com.linkedlogics.sms.smstpdu;

public interface SmsTpdu {

    public SmsTpduType getSmsTpduType();

    public byte[] encodeData() throws Exception;
}
