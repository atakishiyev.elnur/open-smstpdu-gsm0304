package com.linkedlogics.sms.smstpdu;

public interface ConcatenatedShortMessagesIdentifier extends UserDataHeaderElement {

    public boolean getReferenceIs16bit();

    public int getReference();

    public int getMesageSegmentCount();

    public int getMesageSegmentNumber();

    public byte[] getEncodedInformationElementData();

}
