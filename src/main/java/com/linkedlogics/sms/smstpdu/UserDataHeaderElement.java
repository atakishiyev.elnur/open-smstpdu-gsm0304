package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;

public interface UserDataHeaderElement  extends Serializable{

    public int getEncodedInformationElementIdentifier();

    public byte[] getEncodedInformationElementData();
}
