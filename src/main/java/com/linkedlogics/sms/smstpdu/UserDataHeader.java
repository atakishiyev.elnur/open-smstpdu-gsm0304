package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;
import java.util.Map;

public interface UserDataHeader extends Serializable{

    public static int _InformationElementIdentifier_ConcatenatedShortMessages8bit = 0x00;
    public static int _InformationElementIdentifier_ConcatenatedShortMessages16bit = 0x08;
    public static int _InformationElementIdentifier_NationalLanguageSingleShift = 0x24;
    public static int _InformationElementIdentifier_NationalLanguageLockingShift = 0x25;

    public byte[] getEncodedData();

    public Map<Integer, byte[]> getAllData();

    public void addInformationElement(int informationElementIdentifier, byte[] encodedData);

    public void addInformationElement(UserDataHeaderElement informationElement);

    public byte[] getInformationElementData(int informationElementIdentifier);

    public NationalLanguageLockingShiftIdentifier getNationalLanguageLockingShift();

    public NationalLanguageSingleShiftIdentifier getNationalLanguageSingleShift();

    public ConcatenatedShortMessagesIdentifier getConcatenatedShortMessagesIdentifier();
}
