package com.linkedlogics.sms.smstpdu;

public interface SmsDeliverTpdu extends SmsTpdu {

    public boolean getMoreMessagesToSend();

    public boolean getForwardedOrSpawned();

    public boolean getReplyPathExists();

    public boolean getUserDataHeaderIndicator();

    public boolean getStatusReportIndication();

    public AddressField getOriginatingAddress();

    public ProtocolIdentifier getProtocolIdentifier();

    public DataCodingScheme getDataCodingScheme();

    public AbsoluteTimeStamp getServiceCentreTimeStamp();

    public int getUserDataLength();

    public UserData getUserData();
}
