package com.linkedlogics.sms.smstpdu;

import java.io.IOException;
import java.nio.charset.Charset;
import org.mobicents.protocols.asn.AsnInputStream;
import org.mobicents.protocols.asn.AsnOutputStream;

public class SmsDeliverTpduImpl extends SmsTpduImpl implements SmsDeliverTpdu {

    private boolean moreMessagesToSend;
    private boolean forwardedOrSpawned;
    private boolean replyPathExists;
    private boolean userDataHeaderIndicator;
    private boolean statusReportIndication;
    private AddressField originatingAddress;
    private ProtocolIdentifier protocolIdentifier;
    private DataCodingScheme dataCodingScheme;
    private AbsoluteTimeStamp serviceCentreTimeStamp;
    private int userDataLength;
    private UserData userData;

    private SmsDeliverTpduImpl() {
        this.tpduType = SmsTpduType.SMS_DELIVER;
        this.mobileOriginatedMessage = false;
    }

    public SmsDeliverTpduImpl(boolean moreMessagesToSend, boolean forwardedOrSpawned, boolean replyPathExists, boolean statusReportIndication,
            AddressField originatingAddress, ProtocolIdentifier protocolIdentifier, AbsoluteTimeStamp serviceCentreTimeStamp, UserData userData) {
        this();

        this.moreMessagesToSend = moreMessagesToSend;
        this.forwardedOrSpawned = forwardedOrSpawned;
        this.replyPathExists = replyPathExists;
        this.statusReportIndication = statusReportIndication;
        this.originatingAddress = originatingAddress;
        this.protocolIdentifier = protocolIdentifier;
        this.serviceCentreTimeStamp = serviceCentreTimeStamp;
        this.userData = userData;
    }

    public SmsDeliverTpduImpl(byte[] data, Charset gsm8Charset) throws Exception {
        this();

        if (data == null) {
            throw new Exception("Error creating a new SmsDeliverTpduImpl instance: data is empty");
        }
        if (data.length < 1) {
            throw new Exception("Error creating a new SmsDeliverTpduImpl instance: data length is equal zero");
        }

        AsnInputStream stm = new AsnInputStream(data);

        int bt = stm.read();
        if ((bt & _MASK_TP_MMS) == 0) {
            this.moreMessagesToSend = true;
        }
        if ((bt & _MASK_TP_LP) != 0) {
            this.forwardedOrSpawned = true;
        }
        if ((bt & _MASK_TP_RP) != 0) {
            this.replyPathExists = true;
        }
        if ((bt & _MASK_TP_UDHI) != 0) {
            this.userDataHeaderIndicator = true;
        }
        if ((bt & _MASK_TP_SRI) != 0) {
            this.statusReportIndication = true;
        }

        this.originatingAddress = AddressFieldImpl.createMessage(stm);

        bt = stm.read() & 0xFF;
        if (bt == -1) {
            throw new Exception("Error creating a new SmsDeliverTpduImpl instance: protocolIdentifier field has not been found");
        }
        this.protocolIdentifier = new ProtocolIdentifierImpl(bt);

        bt = stm.read() & 0xFF;
        if (bt == -1) {
            throw new Exception("Error creating a new SmsDeliverTpduImpl instance: dataCodingScheme field has not been found");
        }
        this.dataCodingScheme = new DataCodingSchemeImpl(bt);

        this.serviceCentreTimeStamp = AbsoluteTimeStampImpl.createMessage(stm);

        this.userDataLength = stm.read() & 0xFF;
        if (this.userDataLength == -1) {
            throw new Exception("Error creating a new SmsDeliverTpduImpl instance: userDataLength field has not been found");
        }

        int avail = stm.available();
        byte[] buf = new byte[avail];
        try {
            stm.read(buf);
        } catch (IOException e) {
            throw new Exception("IOException while creating a new SmsDeliverTpduImpl instance: " + e.getMessage(), e);
        }
        userData = new UserDataImpl(buf, dataCodingScheme, userDataLength, userDataHeaderIndicator, gsm8Charset);
    }

    @Override
    public boolean getMoreMessagesToSend() {
        return this.moreMessagesToSend;
    }

    @Override
    public boolean getForwardedOrSpawned() {
        return this.forwardedOrSpawned;
    }

    @Override
    public boolean getReplyPathExists() {
        return this.replyPathExists;
    }

    @Override
    public boolean getUserDataHeaderIndicator() {
        return this.userDataHeaderIndicator;
    }

    @Override
    public boolean getStatusReportIndication() {
        return this.statusReportIndication;
    }

    @Override
    public AddressField getOriginatingAddress() {
        return originatingAddress;
    }

    @Override
    public ProtocolIdentifier getProtocolIdentifier() {
        return protocolIdentifier;
    }

    @Override
    public DataCodingScheme getDataCodingScheme() {
        return dataCodingScheme;
    }

    @Override
    public AbsoluteTimeStamp getServiceCentreTimeStamp() {
        return serviceCentreTimeStamp;
    }

    @Override
    public int getUserDataLength() {
        return userDataLength;
    }

    @Override
    public UserData getUserData() {
        return userData;
    }

    @Override
    public byte[] encodeData() throws Exception {

        if (this.originatingAddress == null || this.protocolIdentifier == null || this.serviceCentreTimeStamp == null || this.userData == null) {
            throw new Exception("Parameters originatingAddress, protocolIdentifier, serviceCentreTimeStamp and userData must not be null");
        }

        this.userData.encode();
        this.userDataHeaderIndicator = this.userData.getEncodedUserDataHeaderIndicator();
        this.userDataLength = this.userData.getEncodedUserDataLength();
        this.dataCodingScheme = this.userData.getDataCodingScheme();

        if (this.userData.getEncodedData().length > _UserDataLimit) {
            throw new Exception("User data field length may not increase " + _UserDataLimit);
        }

        AsnOutputStream res = new AsnOutputStream();

        // byte 0
        res.write(SmsTpduType.SMS_DELIVER.getEncodedValue() | (!this.moreMessagesToSend ? _MASK_TP_MMS : 0) | (this.forwardedOrSpawned ? _MASK_TP_LP : 0)
                | (this.replyPathExists ? _MASK_TP_RP : 0) | (this.userDataHeaderIndicator ? _MASK_TP_UDHI : 0)
                | (this.statusReportIndication ? _MASK_TP_SRI : 0));

        this.originatingAddress.encodeData(res);
        res.write(this.protocolIdentifier.getCode());
        res.write(this.dataCodingScheme.getCode());
        this.serviceCentreTimeStamp.encodeData(res);
        res.write(this.userDataLength);
        res.write(this.userData.getEncodedData());
        return res.toByteArray();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("SMS-DELIVER tpdu [");

        boolean started = false;
        if (this.moreMessagesToSend) {
            sb.append("moreMessagesToSend");
            started = true;
        }
        if (this.forwardedOrSpawned) {
            if (started) {
                sb.append(", ");
            }
            sb.append("LoopPrevention");
            started = true;
        }
        if (this.replyPathExists) {
            if (started) {
                sb.append(", ");
            }
            sb.append("replyPathExists");
            started = true;
        }
        if (this.userDataHeaderIndicator) {
            if (started) {
                sb.append(", ");
            }
            sb.append("userDataHeaderIndicator");
            started = true;
        }
        if (this.statusReportIndication) {
            if (started) {
                sb.append(", ");
            }
            sb.append("statusReportIndication");
            started = true;
        }
        if (this.originatingAddress != null) {
            if (started) {
                sb.append(", ");
            }
            sb.append("originatingAddress [");
            sb.append(this.originatingAddress.toString());
            sb.append("]");
        }
        if (this.protocolIdentifier != null) {
            sb.append(", ");
            sb.append(this.protocolIdentifier.toString());
        }
        if (this.serviceCentreTimeStamp != null) {
            sb.append(", serviceCentreTimeStamp [");
            sb.append(this.serviceCentreTimeStamp.toString());
            sb.append("]");
        }
        if (this.userData != null) {
            sb.append("\nMSG [");
            try {
                this.userData.decode();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            sb.append(this.userData.toString());
            sb.append("]");
        }

        sb.append("]");

        return sb.toString();
    }
}
