package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;
import org.mobicents.protocols.asn.AsnOutputStream;

public interface AbsoluteTimeStamp extends Serializable {

    public int getYear();

    public int getMonth();

    public int getDay();

    public int getHour();

    public int getMinute();

    public int getSecond();

    /**
     * @return the timeZone in in quarters of an hour
     */
    public int getTimeZone();

    public void encodeData(AsnOutputStream stm) throws Exception;
}
