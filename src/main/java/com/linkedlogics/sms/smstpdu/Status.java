package com.linkedlogics.sms.smstpdu;

public interface Status {

    // Short message received by the SME
    public static final int SMS_RECEIVED = 0;
    // Short message forwarded by the SC to the SME but the SC is unable to
    // confirm delivery
    public static final int SMS_UNABLE_TO_CONFIRM_DELIVERY = 1;
    // Short message replaced by the SC
    public static final int SMS_REPLACED_BY_SC = 2;
    public static final int CONGESTION_STILL_TRYING = 32;
    public static final int SME_BUSY_STILL_TRYING = 33;
    public static final int NO_REPOSNE_FROM_SME_STILL_TRYING = 34;
    public static final int SERVICE_REJECTED_STILL_TRYING = 35;
    public static final int QUALITY_OF_SERVICE_NOT_AVAILABLE_STILL_TRYING = 36;
    public static final int ERROR_IN_SME_STILL_TRYING = 37;
    public static final int REMOTE_PROCEDURE_ERROR = 64;
    public static final int INVOMPATIBLE_DESTINATION = 65;
    public static final int CONNECTION_REJECTED_BY_SME = 66;
    public static final int NOT_OBTAINABLE = 67;
    public static final int QOS_NOT_AVAILABLE = 68;
    public static final int NO_INTERWORKING_AVAILABLE = 69;
    public static final int SMS_VALIDITY_PERIOD_EXPIRED = 70;
    public static final int SMS_DELETED_BY_ORIGINATING_SME = 71;
    public static final int SMS_DELETED_BY_ADMINISTRATOR = 72;
    // The SM may have previously existed in the SC but the SC no longer has
    // knowledge of it or the SM may never have previously existed in the SC
    public static final int SMS_DOES_NOT_EXIST = 73;
    public static final int CONGESTION = 96;
    public static final int SME_BUSY = 97;
    public static final int NO_REPOSNE_FROM_SME = 98;
    public static final int SERVICE_REJECTED = 99;
    public static final int QUALITY_OF_SERVICE_NOT_AVAILABLE = 100;
    public static final int ERROR_IN_SME = 101;

    public int getCode();
}
