package com.linkedlogics.sms.smstpdu;

public interface ValidityEnhancedFormatData {

    public byte[] getData();
}
