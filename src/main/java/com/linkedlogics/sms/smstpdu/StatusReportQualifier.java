package com.linkedlogics.sms.smstpdu;

public enum StatusReportQualifier {

    SmsSubmitResult(0),
    SmsCommandResult(1);
    private int code;

    private StatusReportQualifier(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static StatusReportQualifier getInstance(int code) {
        switch (code) {
            case 0:
                return SmsSubmitResult;
            default:
                return SmsCommandResult;
        }
    }
}
