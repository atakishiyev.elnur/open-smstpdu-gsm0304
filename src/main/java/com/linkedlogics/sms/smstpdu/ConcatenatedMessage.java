package com.linkedlogics.sms.smstpdu;

public interface ConcatenatedMessage {

    public String getText();

    public UserDataHeaderElement[] getExtraUserDataHeader();

}
