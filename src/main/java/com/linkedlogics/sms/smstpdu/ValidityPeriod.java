package com.linkedlogics.sms.smstpdu;

public interface ValidityPeriod {

    public ValidityPeriodFormat getValidityPeriodFormat();

    public Integer getRelativeFormatValue();

    public Double getRelativeFormatHours();

    public AbsoluteTimeStamp getAbsoluteFormatValue();

    public ValidityEnhancedFormatData getEnhancedFormatValue();
}
