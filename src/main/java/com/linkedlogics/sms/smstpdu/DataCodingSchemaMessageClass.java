

package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;

public enum DataCodingSchemaMessageClass implements Serializable{
	Class0(0),
	Class1(1),
	Class2(2),
	Class3(3);

	private int code;

	private DataCodingSchemaMessageClass(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static DataCodingSchemaMessageClass getInstance(int code) {
		switch (code) {
		case 0:
			return Class0;
		case 1:
			return Class1;
		case 2:
			return Class2;
		default:
			return Class3;
		}
	}
}

