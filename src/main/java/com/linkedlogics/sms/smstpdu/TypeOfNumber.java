package com.linkedlogics.sms.smstpdu;

public enum TypeOfNumber {

    Unknown(0),
    InternationalNumber(1),
    NationalNumber(2),
    NetworkSpecificNumber(3),
    SubscriberNumber(4),
    Alphanumeric(5),
    AbbreviatedNumber(6),
    Reserved(7);
    private int code;

    private TypeOfNumber(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static TypeOfNumber getInstance(int code) {
        switch (code) {
            case 0:
                return Unknown;
            case 1:
                return InternationalNumber;
            case 2:
                return NationalNumber;
            case 3:
                return NetworkSpecificNumber;
            case 4:
                return SubscriberNumber;
            case 5:
                return Alphanumeric;
            case 6:
                return AbbreviatedNumber;
            default:
                return Reserved;
        }
    }
}
