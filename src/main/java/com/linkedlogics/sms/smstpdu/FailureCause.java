package com.linkedlogics.sms.smstpdu;

public interface FailureCause {

    public int getCode();
}
