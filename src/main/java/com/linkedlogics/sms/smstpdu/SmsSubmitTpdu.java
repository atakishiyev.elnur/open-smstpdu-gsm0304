package com.linkedlogics.sms.smstpdu;

public interface SmsSubmitTpdu extends SmsTpdu {

    public boolean getRejectDuplicates();

    public ValidityPeriodFormat getValidityPeriodFormat();

    public boolean getReplyPathExists();

    public boolean getUserDataHeaderIndicator();

    public boolean getStatusReportRequest();

    public int getMessageReference();

    public AddressField getDestinationAddress();

    public ProtocolIdentifier getProtocolIdentifier();

    public DataCodingScheme getDataCodingScheme();

    public ValidityPeriod getValidityPeriod();

    public int getUserDataLength();

    public UserData getUserData();
}
