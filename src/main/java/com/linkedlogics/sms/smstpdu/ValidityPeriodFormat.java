package com.linkedlogics.sms.smstpdu;

public enum ValidityPeriodFormat {

    fieldNotPresent(0),
    fieldPresentEnhancedFormat(1),
    fieldPresentRelativeFormat(2),
    fieldPresentAbsoluteFormat(3);
    private int code;

    private ValidityPeriodFormat(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public static ValidityPeriodFormat getInstance(int code) {
        switch (code) {
            case 0:
                return fieldNotPresent;
            case 1:
                return fieldPresentEnhancedFormat;
            case 2:
                return fieldPresentRelativeFormat;
            default:
                return fieldPresentAbsoluteFormat;
        }
    }
}
