package com.linkedlogics.sms.smstpdu;

import org.mobicents.protocols.asn.AsnOutputStream;

public class TbcdStringWithFiller extends TbcdString {

    protected static int DIGIT_MASK = 0xFF;

    public TbcdStringWithFiller(int minLength, int maxLength, String _PrimitiveName) {
        super(minLength, maxLength, _PrimitiveName);
    }

    public TbcdStringWithFiller(int minLength, int maxLength, String _PrimitiveName, String data) {
        super(minLength, maxLength, _PrimitiveName, data);
    }

    @Override
    public void encodeData(AsnOutputStream asnOs) throws Exception {

        if (this.data == null) {
            throw new Exception("Error while encoding the " + _PrimitiveName + ": data is not defined");
        }

        encodeString(asnOs, this.data);
        this.encodeFiller(asnOs);
    }

    public void encodeFiller(AsnOutputStream asnOs) throws Exception {

        for (int i = data.length() + 1; i < this.maxLength * 2; i = i + 2) {
            asnOs.write(DIGIT_MASK);
        }

    }
}
