package com.linkedlogics.sms.smstpdu;

public interface CommandData {

    public byte[] getEncodedData();

    public String getDecodedMessage();

    public void encode() throws Exception;

    public void decode() throws Exception;
}
