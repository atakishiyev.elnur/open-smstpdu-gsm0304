package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;


public interface DataCodingScheme extends Serializable {

    public int getCode();

    public DataCodingGroup getDataCodingGroup();

    public DataCodingSchemaMessageClass getMessageClass();

    public DataCodingSchemaIndicationType getDataCodingSchemaIndicationType();

    public Boolean getSetIndicationActive();

    public CharacterSet getCharacterSet();

    public boolean getIsCompressed();
}
