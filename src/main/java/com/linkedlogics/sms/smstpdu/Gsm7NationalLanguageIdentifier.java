package com.linkedlogics.sms.smstpdu;

import com.linkedlogics.sms.datacoding.NationalLanguageIdentifier;

public interface Gsm7NationalLanguageIdentifier extends UserDataHeaderElement {

    public NationalLanguageIdentifier getNationalLanguageIdentifier();

    @Override
    public byte[] getEncodedInformationElementData();
}
