package com.linkedlogics.sms.smstpdu;

public class FailureCauseImpl implements FailureCause {

    private int code;

    public FailureCauseImpl(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("TP-Failure-Cause [");
        sb.append(this.code);
        sb.append("]");

        return sb.toString();
    }
}
