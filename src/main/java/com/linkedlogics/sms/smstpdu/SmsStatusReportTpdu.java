package com.linkedlogics.sms.smstpdu;

public interface SmsStatusReportTpdu extends SmsTpdu {

    public boolean getUserDataHeaderIndicator();

    public boolean getMoreMessagesToSend();

    public boolean getForwardedOrSpawned();

    public StatusReportQualifier getStatusReportQualifier();

    public int getMessageReference();

    public AddressField getRecipientAddress();

    public AbsoluteTimeStamp getServiceCentreTimeStamp();

    public AbsoluteTimeStamp getDischargeTime();

    public Status getStatus();

    public ParameterIndicator getParameterIndicator();

    public ProtocolIdentifier getProtocolIdentifier();

    public DataCodingScheme getDataCodingScheme();

    public int getUserDataLength();

    public UserData getUserData();
}
