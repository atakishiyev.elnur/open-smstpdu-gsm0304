package com.linkedlogics.sms.smstpdu;

public interface SmsDeliverReportTpdu extends SmsTpdu {

    public boolean getUserDataHeaderIndicator();

    public FailureCause getFailureCause();

    public ParameterIndicator getParameterIndicator();

    public ProtocolIdentifier getProtocolIdentifier();

    public DataCodingScheme getDataCodingScheme();

    public int getUserDataLength();

    public UserData getUserData();
}
