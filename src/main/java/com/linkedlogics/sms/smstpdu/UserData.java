package com.linkedlogics.sms.smstpdu;

import java.io.Serializable;

public interface UserData extends Serializable {

    public DataCodingScheme getDataCodingScheme();

    public byte[] getEncodedData();

    public boolean getEncodedUserDataHeaderIndicator();

    public int getEncodedUserDataLength();

    public String getDecodedMessage();

    public UserDataHeader getDecodedUserDataHeader();

    public void encode() throws Exception;

    public void decode() throws Exception;
}
