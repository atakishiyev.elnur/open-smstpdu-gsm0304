package com.linkedlogics.sms.datacoding;

import java.io.Serializable;
import com.linkedlogics.sms.smstpdu.CharacterSet;
import com.linkedlogics.sms.smstpdu.DataCodingSchemaMessageClass;

public interface CBSDataCodingScheme extends Serializable {

    public int getCode();

    public CBSDataCodingGroup getDataCodingGroup();

    public CBSNationalLanguage getNationalLanguageShiftTable();

    public CharacterSet getCharacterSet();

    public DataCodingSchemaMessageClass getMessageClass();

    public boolean getIsCompressed();
}
